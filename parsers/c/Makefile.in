# @configure_input@

##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################


# Targets
TARGET_CLIBS = MopsaClangParser
TARGET_BINS = wrapper



# Packs
MopsaClangParser = mopsa_c_parser mopsa_build_db
wrapper = mopsa_build_db Build_wrapper
mopsa_c_parser = Clang_AST Clang_utils Clang_dump Clang_parser \
		Clang_parser_cache C_AST C_print C_utils C_simplify \
		Clang_to_C C_parser 


# C++ files
CC_SRC = mopsa_c_parser/Clang_to_ml.cc


# External packages, as given to ocamlfind
PKGS = str unix zarith


# Flags
OCAMLC = ocamlc -g -cc "$(CXX)"
OCAMLOPT = ocamlopt -cc "$(CXX)"


CLANGRESOURCE := $(shell @CLANG@ -print-resource-dir)

CXXFLAGS := \
	-Wall -Wno-comment\
	-fPIC  \
	-I $(shell ocamlc -where) \
	$(shell @LLVMCONFIG@ --cxxflags) \
	-Wno-strict-aliasing \
	"-DCLANGRESOURCE=\"$(CLANGRESOURCE)\"" \
	$(CXXFLAGS)

LDFLAGS := $(shell @LLVMCONFIG@ --ldflags) $(LDFLAGS)

CCLIBS := \
  @LLVM_CCLIBS@ \
 -lclang \
 $(shell @LLVMCONFIG@ --libs --system-libs) \
 -lstdc++


# MOPSA libraries
MOPSALIBS := utils:MopsaUtils


# MOPSA make rules
MOPSAROOT = @top_srcdir@
include $(MOPSAROOT)/make/main.mk


# Install

inst_src  := $(MOPSAROOT)/bin/mopsa-wrappers
inst_bins := $(foreach f, $(wildcard $(inst_src)/*), $(shell basename $f))

install:
	@mkdir -p @prefix@/bin/mopsa-wrappers
	@for i in $(inst_bins); do \
		echo -e "$(INSTALLMSG)     bin/mopsa-wrappers/$$i"; \
		if [ $$i = "mopsa-wrapper" ]; then \
			cp -f $(inst_src)/$$i @prefix@/bin/mopsa-wrappers/$$i; \
		else \
			ln -s -f @prefix@/bin/mopsa-wrappers/mopsa-wrapper @prefix@/bin/mopsa-wrappers/$$i; \
		fi; \
	done

uninstall:
	@echo -e "$(UINSTALLMSG)     bin/mopsa-wrappers"
	@rm -rf @prefix@/bin/mopsa-wrappers


